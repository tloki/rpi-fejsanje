#!/usr/bin/env python3

from picamera import PiCamera
from scipy import spatial
import _pickle
import openface
import cv2
from FaceRecognition import *

"""
Use this file interactively, like
$ python3
>>> from PhotoSession import *

it'll take some time till al NN models are loaded
"""

# TODO: do I cPickle these models, or maybe load them dynamically (as they are needed)?

PHOTO_DB_FILE = "photoDummyDB.pickle"
TMP_IMG = "sample_images/tmp.jpg"

haar_face_detector = cv2.CascadeClassifier(FACE_CASCADE_PATH)
align = openface.AlignDlib(DLIB_FACE_PREDICTOR_MODEL_PATH)
net = openface.TorchNeuralNet(FACE_EMBEDDING_MODEL_PATH, IMG_DIM_PARAM)
camera = PiCamera()

# db_names:   id -> name
# db_vectors: id -> list(vector)

db_names = {}
db_vectors = {}

try:
    with open(PHOTO_DB_FILE, "rb") as dbfile:
        db_names, db_vectors = _pickle.load(dbfile)
except FileNotFoundError:
    with open(PHOTO_DB_FILE, "wb") as dbfile:
        _pickle.dump(({}, {}), dbfile)


def add_employee(name):
    name = name.strip()

    while "  " in name:
        name = name.replace("  ", " ")

    if name.upper() in map(lambda x: x.upper(), db_names.values()):
        raise RuntimeError("name already exists")

    id = None

    if len(db_names) == 0:
        id = 0
    else:
        id = max(db_names.keys()) + 1

    db_names[id] = name
    db_vectors[id] = []

    with open(PHOTO_DB_FILE, "wb") as file:
        _pickle.dump((db_names, db_vectors), file)

    return id


def list_employees():
    for id in db_names:
        print("Name:", db_names[id], "ID:", id)


def add_photo(id, preview=False, timeout=3, img_path=None):
    if isinstance(id, str):
        id = name_to_id(id)

    if preview:
        camera.start_preview()

    # interactive mode:
    if img_path is None:
        for i in range(timeout, 0, -1):
            print("taking photo in {}...".format(i))

        camera.capture(TMP_IMG)
        print("photo is taken")
        img_path = TMP_IMG

    if preview:
        camera.stop_preview()

    rgb_image = get_rgb_image_from_file(img_path)
    bb = find_face_fast(rgb_image, haar_face_detector)

    if len(bb) == 0:
        print("no faces found!")
        return False

    bb = bb[0]  # find only a single face

    aligned_face = align_face(rgb_image, align, IMG_DIM_PARAM, bb)
    rep = generate_rep_vector(aligned_face, net)

    db_vectors[id].append(rep)

    with open(PHOTO_DB_FILE, "wb") as file:
        _pickle.dump((db_names, db_vectors), file)

    return True


# TODO: refactor
def name_to_id(name):
    id = name

    if id.strip().upper() not in map(lambda x: x.upper(), db_names.values()):
        raise RuntimeError("No such employee in DB")

    for ids in db_names:
        if db_names[ids].upper() == id.strip().upper():
            id = ids
            break

    if isinstance(id, str):
        raise RuntimeError("No such employee in DB")

    if id not in db_names:
        raise RuntimeError("No such employee in DB")

    return id


def drop_db():
    global db_names, db_vectors
    db_names = {}
    db_vectors = {}
    with open(PHOTO_DB_FILE, "wb") as file:
        _pickle.dump((db_names, db_vectors), file)


def get_arrays_from_db():
    import numpy as np

    # id-jevi po broju vektora...

    ids = sorted(db_names.keys())

    idv = []
    vs = []
    for id in ids:
        for i in range(len(db_vectors[id])):
            idv.append(id)
            vs.append(db_vectors[id][i])

    # idv: ids of all of vectors in vs
    # vectors of every photo taken in database
    return idv, np.array(vs)


def cos_distance_with_db(rep_vector, all_vectors, idv=None):
    cos_distances = np.apply_along_axis(lambda x: spatial.distance.cosine(x, rep_vector), 1, all_vectors)

    if idv is None:
        return cos_distances  # shaped (N, ), where N is number of photos in DB

    return cos_distances  # TODO: maybe glue idv and similairity?


if __name__ == '__main__':
    # loop over photos in db and print their cos distance with new photo
    # TODO: implement this for non-rpi camera?

    capture = None
    frame = None
    capture_process = None
    capture_generator = None

    # camera init
    camera.close()
    rpi_camera = RasPiCamera(CAMERA_CAPTURE_WIDTH, CAMERA_CAPTURE_HEIGHT, CAMERA_MAX_FRAME_RATE)
    rpi_camera.capture()
    time.sleep(2)

    # start the main loop:
    fps_counter = FPSCounter(report_time=1.0, event_function=print_fps)

    # fetch whole database in performance efficient format to keep it in RAM:
    idv, vector_db = get_arrays_from_db()

    while True:

        frame = rpi_camera.capture()

        detected_bbox = detect_and_draw(frame, haar_face_detector, HAAR_FLAGS, HAAR_SCALE, MIN_NEIGHBOURS, MIN_SIZE,
                                        PAN, SMALL_WIDTH, VERBOSE, display=DISPLAY)

        if len(detected_bbox) > 0:
            bbox1 = detected_bbox[0]
            unknown_rgb_face = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            unknown_aligned_face = align_face(unknown_rgb_face, align, IMG_DIM_PARAM, detected_bbox[0])
            unknown_rep = generate_rep_vector(unknown_aligned_face, net)

            dist_matrix = cos_distance_with_db(unknown_rep, vector_db)
            print(dist_matrix)

            i = np.argmin(dist_matrix)
            dist = np.min(dist_matrix)
            id = idv[i]
            name = db_names[id]

            if dist < 0.45:
                print("This is likely: {} (min cos distance of {})".format(name, dist))
            else:
                print("unknown individual found [not in database]")
                print("this individual is most similar with individual: {} with cosine distance at {}".format(name,
                                                                                                              dist))

        # fps counter
        if VERBOSE:
            fps_counter.new_lap()
