#!/usr/bin/env python3
import cv2
from dlib import rectangle
import numpy as np


def detect_and_draw(img: np.ndarray, cascade_classifier: cv2.CascadeClassifier,
                    cascade_flags, scale_factor, min_neighbours, min_size, pan, small_width, verbose=False,
                    input_color_space=cv2.COLOR_BGR2GRAY, display=False):

    height, width = img.shape[:2]

    # TODO: make use of this allocation!
    # allocate temporary images
    gray = np.zeros((width, height), dtype=np.uint8)

    image_scale = width / small_width

    # scale image to reduce processing needs
    small_img = np.zeros((int(height / image_scale), int(width / image_scale)), np.uint8)
    small_height, small_width = small_img.shape[:2]

    # convert color input image to grayscale
    gray = cv2.cvtColor(src=img, code=input_color_space, dstCn=1)

    # scale input image for faster processing
    small_img = cv2.resize(src=gray, interpolation=cv2.INTER_LINEAR, dsize=(small_width, small_height))

    # apply histogram equalisation for better recognition in dark and light environments
    cv2.equalizeHist(small_img, small_img)  # TODO: check if this call by reference works...

    # detect faces
    faces = cascade_classifier.detectMultiScale(image=small_img, scaleFactor=scale_factor, minNeighbors=min_neighbours,
                                                flags=cascade_flags, minSize=min_size)

    bboxes = []

    if len(faces) != 0:
        # if sth_detected == 0:
        #     sth_detected = 1

        if verbose:
            print("faces: {} ".format(len(faces)))

        for ith_face in range(faces.shape[0]):

            x, y, w, h = faces[ith_face, ]
            # the input to cv.HaarDetectObjects was resized, so scale the
            # bounding box of each face and convert it to two CvPoints
            pt1 = (int(x * image_scale), int(y * image_scale))
            pt2 = (int((x + w) * image_scale), int((y + h) * image_scale))

            bbox = ([pt1, pt2])
            bbox = rectangle(left=bbox[0][0], top=bbox[0][1], right=bbox[1][0], bottom=bbox[1][1])

            bboxes.append(bbox)

            if display:
                cv2.rectangle(img=img, pt1=pt1, pt2=pt2, color=(255, 0, 0), thickness=3)

            # find amount needed to pan/tilt
            # TODO: what is this about? Document it!
            # span = pt1[0]
            # mid = small_width / 2
            #
            # if span < mid:
            #     pandir = -1
            # else:
            #     pandir = 1
            #
            # pan = pan + pandir
            # if pan > 180:
            #     pan = 180
            # if pan < 0:
            #     pan = 0

    if display:
        cv2.imshow("result", img)

    # else:
    #
    #     if sth_detected == 1:
    #         # print "Last seen at: ", pt1[0], ",", pt2[0], "\t", pt1[1], ",", pt2[1]
    #         # status = "just disappeared"
    #         TODO: no 'habla' dis
    #         sth_detected = 0
    # return sth_detected

    return bboxes
