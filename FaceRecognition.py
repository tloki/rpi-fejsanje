#!/usr/bin/env python3
import itertools
import openface
from main import *


VERBOSE = True
DLIB_FACE_PREDICTOR_MODEL_PATH = "trainedModels/shape_predictor_68_face_landmarks.dat"
FACE_EMBEDDING_MODEL_PATH = "trainedModels/nn4.small2.v1.arm.t7"
IMG_DIM_PARAM = 96
NP_PRINT_PRECISION = 2

np.set_printoptions(precision=NP_PRINT_PRECISION)


def get_rgb_image_from_file(image_path):
    if VERBOSE:
        print("Processing {}.".format(image_path))
    bgr_image = cv2.imread(image_path)
    if bgr_image is None:
        raise Exception("Unable to load image: {}".format(image_path))
    rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)
    if VERBOSE:
        print("  + Original size: {}".format(rgb_image.shape))
    return rgb_image


def find_face(rgb_image, face_detector):
    start_time = time.time()
    bb = face_detector.getLargestFaceBoundingBox(rgb_image)
    if bb is None:
        raise Exception("Unable to find a face!")
    if VERBOSE:
        print("  + Face detection took {} seconds.".format(time.time() - start_time))
        print("bbox: {}".format(bb))
    return bb


def find_face_fast(rgb_image, face_detector):
    start_time = time.time()
    bb2 = detect_and_draw(rgb_image, face_detector, HAAR_FLAGS, HAAR_SCALE, MIN_NEIGHBOURS, MIN_SIZE, PAN, SMALL_WIDTH,
                          VERBOSE, cv2.COLOR_RGB2GRAY, DISPLAY)

    if VERBOSE and len(bb2) == 0:
        print("unable to find faces!")

    if VERBOSE:
        print("  + Face detection took {} seconds.".format(time.time() - start_time))
        print("bbox: {}".format(bb2))
    return bb2


def align_face(rgb_image, face_align_model, image_dim, bbox, landmark_indices=openface.AlignDlib.OUTER_EYES_AND_NOSE):
    start_time = time.time()
    found_face = face_align_model.align(image_dim, rgb_image, bbox,
                                        landmarkIndices=landmark_indices)
    if found_face is None:
        raise Exception("Unable to align image!")
    if VERBOSE:
        print("  + Face alignment took {} seconds.".format(time.time() - start_time))

    return found_face


def generate_rep_vector(aligned_face, alignment_model):
    start_time = time.time()
    representation_vector = alignment_model.forward(aligned_face)
    if VERBOSE:
        print("  + OpenFace forward pass took {} seconds.".format(time.time() - start_time))
        print("Representation:")
        print(representation_vector)
        print("-----\n")
    return representation_vector


# def get_rep(image_path):
#     if VERBOSE:
#         print("Processing {}.".format(image_path))
#     bgr_image = cv2.imread(image_path)
#     if bgr_image is None:
#         raise Exception("Unable to load image: {}".format(image_path))
#     rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)
#     if VERBOSE:
#         print("  + Original size: {}".format(rgb_image.shape))
#
#     print(
#         """"############################
#         #### SLOW ALGO #############
#         ############################""")
#
#     start_time = time.time()
#     bb = align.getLargestFaceBoundingBox(rgb_image)
#     if bb is None:
#         raise Exception("Unable to find a face: {}".format(image_path))
#     if VERBOSE:
#         print("  + Face detection took {} seconds.".format(time.time() - start_time))
#         print("bbox: {}".format(bb))
#         print(type(bb))
#
#     start_time = time.time()
#     aligned_face = align.align(IMG_DIM_PARAM, rgb_image, bb,
#                                landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
#     if aligned_face is None:
#         raise Exception("Unable to align image: {}".format(image_path))
#     if VERBOSE:
#         print("  + Face alignment took {} seconds.".format(time.time() - start_time))
#
#     start_time = time.time()
#     rep = net.forward(aligned_face)
#     if VERBOSE:
#         print("  + OpenFace forward pass took {} seconds.".format(time.time() - start_time))
#         print("Representation:")
#         print(rep)
#         print("-----\n")
#
#     print(
#     """"############################
#     #### FAST ALGO #############
#     ############################""")
#
#     start_time = time.time()
#     bb2 = detect_and_draw(rgb_image, face_detector)
#     # print(bb2[0][0], bb2[1][0], bb2[1][0], bb2[0][1])
#     if bb2 != []:
#         bb2 = dlib.rectangle(left=bb2[0][0], top=bb2[0][1], right=bb2[1][0], bottom=bb2[1][1])
#
#     if VERBOSE:
#         print("  + Face detection took {} seconds.".format(time.time() - start_time))
#         print("bbox: {}".format(bb2))
#
#     start_time = time.time()
#     aligned_face2 = align.align(IMG_DIM_PARAM, rgb_image, bb2,
#                                 landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
#     if aligned_face2 is None:
#         raise Exception("Unable to align image: {}".format(image_path))
#     if VERBOSE:
#         print("  + Face alignment took {} seconds.".format(time.time() - start_time))
#
#     rep2 = net.forward(aligned_face2)
#     if VERBOSE:
#         print("  + OpenFace forward pass took {} seconds.".format(time.time() - start_time))
#         print("Representation:")
#         print(rep2)
#         print("-----\n")
#
#     print("L2 diff between fast and slow algo: {}".format(np.dot(rep2, rep)))
#
#     return rep


if __name__ == '__main__':

    images = ["sample_images/loki1.jpg", "sample_images/loki2.jpg",
              "sample_images/lovre1.jpg", "sample_images/lovre2.jpg"]

    start = time.time()
    haar_face_detector = cv2.CascadeClassifier(FACE_CASCADE_PATH)
    if VERBOSE:
        print("Loading the Haar face detector model took {} seconds.".format(
            time.time() - start))

    start = time.time()
    align = openface.AlignDlib(DLIB_FACE_PREDICTOR_MODEL_PATH)
    if VERBOSE:
        print("Loading the dlib face alignment and detector model took {} seconds.".format(
            time.time() - start))

    start = time.time()
    net = openface.TorchNeuralNet(FACE_EMBEDDING_MODEL_PATH, IMG_DIM_PARAM)
    if VERBOSE:
        print("Loading the openface face-embedding neural-net model took {} seconds.".format(
            time.time() - start))

    # # SLOW
    # rep_images = []
    # for image in images:
    #     rgb_image = get_rgb_image_from_file(image)
    #     bb = find_face(rgb_image, align)
    #     if bb is None:
    #         rep_images.append((image, None))
    #         continue
    #     aligned_face = align_face(rgb_image, align, imgDim, bb)
    #     rep = generate_rep_vector(aligned_face, net)
    #     rep_images.append((image, rep))

    # FAST
    rep_images_fast = []
    for image in images:
        rgb_image = get_rgb_image_from_file(image)
        bb = find_face_fast(rgb_image, haar_face_detector)

        if len(bb) == 0:
            rep_images_fast.append((image, None))
            continue

        bb = bb[0]  # find only a single face

        aligned_face = align_face(rgb_image, align, IMG_DIM_PARAM, bb)
        rep = generate_rep_vector(aligned_face, net)
        rep_images_fast.append((image, rep))

    for img1, img2 in itertools.combinations(rep_images_fast, 2):
        if img1[1] is None:
            print("unable to compare {} with {}: {} has no vector rep (unable to find face!)".format(img1[0], img1[0],
                                                                                                     img1[0]))
            continue

        if img2[1] is None:
            print("unable to compare {} with {}: {} has no vector rep (unable to find face!)".format(img1[0], img1[0],
                                                                                                     img2[0]))
            continue

        d = img1[1] - img2[1]
        print("Comparing {} with {}.".format(img1[0], img2[0]))
        print(
            "  + Squared l2 distance between representations: {:0.3f}".format(np.dot(d, d)))

