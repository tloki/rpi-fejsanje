#!/usr/bin/env python3
import platform
from FPSCounter import *
from FastFaceDetecting import *
from FaceRecognition import *
from scipy import spatial


DISPLAY = False
VERBOSE = True

FACE_CASCADE_PATH = "trainedModels/face.xml"

CAMERA_CAPTURE_WIDTH = 320
CAMERA_CAPTURE_HEIGHT = 240
CAMERA_MAX_FRAME_RATE = 120
# not to be confused with scaled-detection photo!


# raspicam hack:
# sudo modprobe bcm2835-v4l2

# Parameters for haar detection
# From the API:
# The default parameters (scale_factor=2, min_neighbors=3, flags=0) are tuned
# for accurate yet slow object detection. For a faster operation on real video
# images the settings are:
# scale_factor=1.2, min_neighbors=2, flags=CV_HAAR_DO_CANNY_PRUNING,
# min_size=<minimum possible face size
MIN_SIZE = (5, 5)
HAAR_SCALE = 1.2
MIN_NEIGHBOURS = 3
HAAR_FLAGS = 0

# detection image width i
# 70 fast frame rate
# 90 - more accurate / smaller faces / further away
# 120+ slow
SMALL_WIDTH = 200
PAN = 100

IS_RPI = False

#####################################
#    DO NOT EDIT BELOW THIS  ########
#####################################


if platform.machine().startswith("arm"):
    from RasPiCamera import RasPiCamera
    IS_RPI = True
    print("Raspberry Pi (ARM) machine detected")
else:
    print("intel X86 machine detected")


if __name__ == '__main__':
    haar_face_detector = cv2.CascadeClassifier(FACE_CASCADE_PATH)
    align = openface.AlignDlib(DLIB_FACE_PREDICTOR_MODEL_PATH)
    net = openface.TorchNeuralNet(FACE_EMBEDDING_MODEL_PATH, IMG_DIM_PARAM)

    if DISPLAY:
        cv2.namedWindow("result")

    rpi_camera = None
    capture = None
    frame = None
    capture_process = None
    capture_generator = None

    if IS_RPI:  # RPi cam init + warmup
        rpi_camera = RasPiCamera(CAMERA_CAPTURE_WIDTH, CAMERA_CAPTURE_HEIGHT, CAMERA_MAX_FRAME_RATE)
        rpi_camera.capture()
        time.sleep(2)

    # TODO: optimize this or delete it
    else:       # usb cam init + warmup
        capture = cv2.VideoCapture(0)
        time.sleep(1)
        ret, frame = capture.read()
        capture.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
        capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)
        time.sleep(1)

    # start the main loop:
    fps_counter = FPSCounter(report_time=1.0, event_function=print_fps)

    # get loki's vector
    loki_rgb = get_rgb_image_from_file("sample_images/Marko_mutna.jpg")
    loki_bb = find_face_fast(loki_rgb, haar_face_detector)[0]
    loki_aligned_face = align_face(loki_rgb, align, IMG_DIM_PARAM, loki_bb)
    loki_rep = generate_rep_vector(loki_aligned_face, net)
    if VERBOSE:
        print("generated reference vector")
        print(loki_rep)

    while True:

        if IS_RPI:
            frame = rpi_camera.capture()
        else:
            ret, frame = capture.read()

        detected_bbox = detect_and_draw(frame, haar_face_detector, HAAR_FLAGS, HAAR_SCALE, MIN_NEIGHBOURS, MIN_SIZE, PAN,
                                        SMALL_WIDTH, VERBOSE, display=DISPLAY)

        if len(detected_bbox) > 0:
            bbox1 = detected_bbox[0]
            unknown_rgb_face = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            unknown_aligned_face = align_face(unknown_rgb_face, align, IMG_DIM_PARAM, detected_bbox[0])
            unknown_rep = generate_rep_vector(unknown_aligned_face, net)
            print(unknown_rep.shape)

            print("L2 distance to Marko: {}".format(np.dot(unknown_rep, loki_rep)))
            print("Cos distance to Marko: {}".format(spatial.distance.cosine(unknown_rep, loki_rep)))

        # exit when any key pressed
        if DISPLAY:
            if cv2.waitKey(1) >= 0:
                break

        if VERBOSE:
            fps_counter.new_lap()

    # uncomment if while loop is not indefinite
    # if DISPLAY:
    #     cv2.destroyWindow("result")
