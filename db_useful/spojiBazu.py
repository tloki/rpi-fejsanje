#!/usr/bin/env python3

#jdbc:sqlserver://praksa2018.database.windows.net:1433;database=praksa2018;user=praksaadmin@praksa2018;password={your_password_here};encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;

import pyodbc
server = 'praksa2018.database.windows.net'
database = 'praksa2018'
username = 'praksaadmin'
password = 'Pr@k$@_Adm!n'
#driver= '{ODBC Driver 13 for SQL Server}'
driver= '{ODBC Driver 17 for SQL Server}'
cnxn = pyodbc.connect('DRIVER='+driver+';SERVER='+server+';PORT=1433;DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()

query = "SELECT * FROM PictureVectors JOIN VectorElements ON VectorElements.PictureVectorId = PictureVectors.Id"

cursor.execute(query)
row = cursor.fetchone()
while row:
    print (str(row[0]) + " " + str(row[1]))
    row = cursor.fetchone()
