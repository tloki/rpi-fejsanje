#!/usr/bin/env python3

from picamera import PiCamera
from picamera.array import PiRGBArray
from FPSCounter import *


# TODO: can this be optimized? A lot of openin/closing is in play right now (more than 50% of CPU cycles...)
class RasPiCamera:

    def __init__(self, width, height, framerate):

        self.camera = PiCamera()
        self.camera.resolution = (width, height)
        self.camera.framerate = framerate
        self.rawCapture = PiRGBArray(self.camera, size=self.camera.resolution)
        # TODO: maybe grayscale image option?
        self.capture_continuous = self.camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True)

    def capture(self):

        frame = next(self.capture_continuous)
        image = self.rawCapture.array
        self.rawCapture.truncate(0)

        return image


if __name__ == "__main__":

    cv2.namedWindow("result")
    camera = RasPiCamera(320, 240, 90)

    time_start = time.time()
    n_iter = 0

    fps = FPSCounter(event_function=show_img, report_time=0.1)

    while True:

        frame = camera.capture()
        fps.new_lap(frame)
