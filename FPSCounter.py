import time
import cv2


class FPSCounter:

    def __init__(self, report_time=1.0, event_function=print):
        self.time_started = time.time()
        self.time_last = time.time()
        self.elapsed = 0
        self.event_counter = 0
        self.report_time = report_time
        self.event_function = event_function

    def start(self):
        self.time_started = time.time()
        self.event_counter = 0

    def new_lap(self, arg=None):
        now = time.time()
        self.elapsed = now - self.time_started
        self.time_last = now
        self.event_counter += 1

        if self.elapsed > self.report_time:
            fps = self.event_counter / self.elapsed
            self.time_started = now
            self.event_counter = 0
            self.time_last = now

            if arg is None:
                self.event_function(fps)
            else:
                self.event_function(fps, arg)


def show_img(fps, image):
    print("FPS: {:.2f}".format(fps))
    cv2.waitKey(1)
    cv2.imshow("result", image)


def print_fps(fps, nonarg=None):
    print("FPS: {:.2f}".format(fps))